#!/usr/bin/env python3

import config
from pynput import keyboard
from time import sleep
import sys,os
from selenium import webdriver
from splinter.browser import Browser
import pyautogui
import base64
from time import sleep
from datetime import date
import pathlib



def on_activate():
    print('Bypass key detected. Exiting launcher.')
    os.system("/usr/bin/startxfce4")
    return False
def for_canonical(f):
    return lambda k: f(l.canonical(k))

if not config.SCREEN['FORCEBYPASS'] :
    # !!!!  HERE BE DRAGONS. FOLLOW NOT AND RETURN TO WHENCE YOU CAME ELSE CHANCE LOSS OF LIMB OR LIFE!!! #
    os.environ["PATH"] += os.pathsep + "/usr/lib/chromium-browser/"  # Help find the chrome driver
    resolution = pyautogui.size()

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option("excludeSwitches", ['enable-automation', 'load-extension'])
    chrome_options.add_argument('--disable-extensions')
    chrome_options.add_argument("--disable-dev-shm-using")
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--kiosk')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--disable-save-password-bubble')
    chrome_options.add_argument('--window-size={},{}'.format(resolution.width, resolution.height))

    browser = Browser('chrome', headless=False, fullscreen=True, incognito=True, options=chrome_options)

    while True:
        try:
            browser.visit("https://whentowork.com/logins.htm")
            browser.find_by_id('username').fill(config.WTW['USERNAME'])
            browser.find_by_id('password').fill(base64.b64decode(config.WTW['PASSWORD']).decode('utf-8'))
            browser.find_by_name('Submit1')[0].click()
            browser.execute_script("ReplWin('empfullschedule','&View=Month');")
            break
        except:
            errorfile = pathlib.Path(__file__).parent.resolve() / "errorpage.htm"
            browser.visit("file://{}".format(errorfile))
            sleep(300)


    while True:
        try:
            today = date.today()
            datestr = today.strftime("%-m/%-d/%Y")

            browser.reload()
            browser.execute_script("window.scrollTo(0,320);")
            browser.execute_script("document.body.style.zoom = '0.75'")
            browser.execute_script("document.querySelectorAll(\"[onclick*='{}']\")[0].scrollIntoView();".format(datestr))
        except:
            pass
        sleep(21600)



else:
    os.system("/usr/bin/startxfce4")



# Watch for hotkey to start XFCE4 proper and a terminal. FOR DEBUGING USE ONLY. NOTHING NEFARIOUS PLEASE!! THANKS!
hotkey = keyboard.HotKey(
    keyboard.HotKey.parse('<ctrl>+<alt>+<esc>'), on_activate)
with keyboard.Listener(on_press=for_canonical(hotkey.press)) as l:
    l.join()
